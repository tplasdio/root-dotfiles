#!/bin/sh
# Aleatorizar dirección MAC
# Dependencies: macchanger, ifconfig or ip, yes

set_interface_switch_cmd() {
	if hash ifconfig >/dev/null 2>&1; then
		interface_switch_cmd(){ ifconfig "$@" ;}
	elif hash ip >/dev/null 2>&1; then
		interface_switch_cmd(){ ip link set dev "$@" ;}
	else
		>&2 printf -- "No hay un comando que apague y prenda interfaces!\nInstale 'ip' o 'ifconfig'"
		return 1
	fi
}

cambiar_mac() {
	_interface="$1"
	# Si la interfaz está prendida, apagarla, cambiar y prenderla
	interface_switch_cmd "${_interface}" down
	# También aleatorizar el bit U/L de la MAC con shuf
	macchanger -r$(shuf -e 'b' '' -n1) "${_interface}"
	# TODO: Solo prenderla si estaba prendida antes
	interface_switch_cmd "${_interface}" up
}

interactivo() {
	# Para cada interfaz excepto loopback, preguntar si desea cambiarle la MAC
	for interface in $(ls /sys/class/net | grep -v 'lo'); do
		printf -- "¿Desea aleatorizar interfaz '\033[1m%s\033[m'? (Sí|no): " "${interface}"
		read -r REPLY
		case "${REPLY}" in
			''|[SsYy]*) cambiar_mac "$interface";;
			[Nn]*) continue;;
		esac
	done
}

main() {
	set_interface_switch_cmd || return $?
	case "$1" in
		-y|--yes) yes | interactivo ;;
		* ) interactivo
	esac
}

main "$@" || exit $?
